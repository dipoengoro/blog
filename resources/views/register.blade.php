<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <!-- First Name -->
        <label for="fname">First name:</label>
        <br><br>
        <input type="text" id="fname" name="fname">
        <br><br>
        <!-- Last Name -->
        <label for="lname">Last name:</label>
        <br><br>
        <input type="text" id="lname" name="lname">
        <br><br>
        <!-- Gender -->
        <label>Gender:</label>
        <br><br>
        <!-- Male -->
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label>
        <br>
        <!-- Female -->
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label>
        <br>
        <!-- Other -->
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label>
        <br><br>
        <!-- Nationality -->
        <label>Nationality:</label>
        <br><br>
        <!-- Dropdown -->
        <select name="nationality">
            <option value="Inggris" selected>Inggris</option>
            <option value="Amerika" selected>Amerika</option>
            <option value="Indonesian" selected>Indonesian</option>
        </select>
        <br><br>
        <!-- Language Spoken -->
        <label>Language Spoken:</label>
        <br><br>
        <!-- Indonesian -->
        <input type="checkbox" id="indonesian" name="lang-spoken" value="Indonesian">
        <label for="indonesian">Bahasa Indonesia</label>
        <br>
        <!-- English -->
        <input type="checkbox" id="english" name="lang-spoken" value="English">
        <label for="english">English</label>
        <br>
        <!-- Other -->
        <input type="checkbox" id="other" name="lang-spoken" value="Other">
        <label for="other">Other</label>
        <br><br>
        <!-- Bio -->
        <label>Bio:</label>
        <br><br>
        <!-- Textarea -->
        <textarea name="bio" cols="40" rows="10"></textarea>
        <br>
        <!-- Submit -->
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>
